/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.converter;

import com.supinfo.supforum.entity.Category;
import com.supinfo.supforum.service.CategoryService;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Mathieu
 */

@ManagedBean
@FacesConverter(value = "categoryConverter")
public class CategoryConverter implements Converter{
    
    @EJB
    CategoryService categoryService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        
        Long id = Long.valueOf(value);
        return categoryService.getCategoryById(id);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        
        Category category = (Category) value;
        return category.getId().toString();
    }
    
}
