/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.controller;

import com.supinfo.supforum.entity.Message;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Mathieu
 */
@ManagedBean
@SessionScoped
public class MessageController {
    
    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;
    
    private Message message;
    
    public MessageController(){
        
    }
    
    public Message getMessage(){
        return this.message;
    }
    
    
    
    public NavigationBean getNavigationBean(){
        return this.navigationBean;
    }
    
    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }
}
