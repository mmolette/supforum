/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.controller;

import com.supinfo.supforum.entity.User;
import static com.supinfo.supforum.filter.AuthFilter.SESSION_USER;
import com.supinfo.supforum.service.UserService;
import com.supinfo.supforum.util.SessionUtil;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mathieu
 */
@ManagedBean
@SessionScoped
public class RegisterController {
    
    @EJB
    UserService userService;
    
    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;
    
    private User user = new User();
    
    public RegisterController(){
        
    }
    
    public void checkUser() throws IOException{
        
        HttpSession session = SessionUtil.getSession();
        if(session.getAttribute(SESSION_USER) != null){
            System.out.println("test");
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/auth/forum.xhtml");
        }
    }
    
    public String register(){
        
        
        User u = new User(user.getUsername(), user.getPassword(), user.getEmail());
        
        try {
            userService.addUser(u);
            navigationBean.redirectToAuthForum();
        } catch (Exception ex) {
            Logger.getLogger(RegisterController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "";
    }
    
    public User getUser(){
        return user;
    }
    
    public NavigationBean getNavigationBean(){
        return this.navigationBean;
    }
    
    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }
}
