/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Mathieu
 */
@ManagedBean
@SessionScoped
public class BackOfficeController {
    
    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;
    
    public NavigationBean getNavigationBean(){
        return this.navigationBean;
    }
    
    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }
    
    
    public BackOfficeController(){
        
    }
    
}
