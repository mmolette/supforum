/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.controller;

import com.supinfo.supforum.service.ForumService;
import com.supinfo.supforum.service.MessageService;
import com.supinfo.supforum.service.UserService;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Mathieu
 */
@ManagedBean
@SessionScoped
public class ForumController {
    
    @EJB
    UserService userService;
    
    @EJB
    MessageService messageService;
    
    @EJB
    ForumService forumService;
    
    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;
    
    public NavigationBean getNavigationBean(){
        return this.navigationBean;
    }
    
    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }
    
    
    public ForumController(){
        
    }
    
    public String getForumCreadted(){
        
        return forumService.getCreatedDate().toString();
    }
    
    public int getNbUsers(){
        return userService.getNbUsers();
    }
    
    public int getNbMessages(){
        return messageService.getNbMessages();
    }
}
