/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.controller;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Mathieu
 */
@ManagedBean
@SessionScoped
public class NavigationBean implements Serializable{
    
    
    /** Public **/
    public String redirectToLogin() {
        return "/login.xhtml?faces-redirect=true";
    }
     
    public String toLogin() {
        return "/login.xhtml";
    }
    
    public String redirectToRegister() {
        return "/register.xhtml?faces-redirect=true";
    }
     
    public String toRegister() {
        return "/register.xhtml";
    }
    
    public String redirectToForum(){
        return "/forum.xhtml?faces-redirect=true";
    }
    
    public String toForum(){
        return "/forum.xhtml";
    }
    
    
    /** Auth **/
    public String redirectToProfile(Long id){
        return "/auth/profile.xhtml?id=" + id + "faces-redirect=true";
    }
    
    public String toProfile(Long id){
        return "/auth/profile.xhtml?id=" + id;
    }
    
    public String redirectToAuthForum(){
        return "/auth/forum.xhtml?faces-redirect=true";
    }
    
    public String toAuthForum(){
        return "/auth/forum.xhtml";
    }
    
    
    public String redirectToAddTopic(){
        return "/auth/addTopic.xhtml?faces-redirect=true";
    }
    
    public String toAddTopic(){
        return "/auth/addTopic.xhtml";
    }
    
    public String redirectToAddMessage(){
        return "/auth/addMessage.xhtml?faces-redirect=true";
    }
    
    public String toAddMessage(){
        return "/auth/addMessage.xhtml";
    }
    
    
    /** Admin */
    
    public String redirectToBackOffice(){
        return "/auth/admin/backoffice.xhtml?faces-redirect=true";
    }
    
    public String redirectToBoardManagement(){
        return "/auth/admin/boardManagement.xhtml?faces-redirect=true";
    }
    
    public String redirectToCategoryManagement(){
        return "/auth/admin/categoryManagement.xhtml?faces-redirect=true";
    }
    
    public String redirectToUserManagement(){
        return "/auth/admin/userManagement.xhtml?faces-redirect=true";
    }
    
    public String redirectToAddCategory(){
        return "/auth/admin/addCategory.xhtml?faces-redirect=true";
    }
    
    public String toAddCategory(){
        return "/auth/admin/addCategory.xhtml";
    }
    
    public String redirectToAddBoard(){
        return "/auth/admin/addBoard.xhtml?faces-redirect=true";
    }
    
    public String toAddBoard(){
        return "/auth/admin/addBoard.xhtml";
    }
    
    
    /** Error */
    
    public String redirectToForbidden(){
        return "/error/403.xhtml?faces-redirect=true";
    }
    
    public String toForbidden(){
        return "/error/403.xhtml";
    }
}
