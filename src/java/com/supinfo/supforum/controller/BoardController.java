/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.controller;

import com.supinfo.supforum.entity.Board;
import com.supinfo.supforum.service.BoardService;
import com.supinfo.supforum.service.CategoryService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Mathieu
 */
@ManagedBean
@SessionScoped
public class BoardController {
    
    @EJB
    BoardService boardService;
    
    @EJB
    CategoryService categoryService;
    
    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;
    
    private List<Board> boards;
    private Board board = new Board();
    
    // Remove
    private Map<Long, Boolean> toRemove = new HashMap<Long, Boolean>();
    
    public BoardController(){
        
    }
    
    public Board getBoard(){
        return this.board;
    }
    
    public Map<Long, Boolean> getToRemove(){
        return this.toRemove;
    }
    
    
    /* Methods */
    
    public void newBoard(){
        
        boardService.addBoard(board);
        categoryService.setCategory(board.getCategory());
    }
    
    public List<Board> getBoards(){
        
        List<Board> boards = boardService.getAllBoards();
        return boards;
    }
    
    public void removeBoard(){
        System.out.println("removeBoard");
        List<Board> boardsToDelete = new ArrayList<>();
        List<Long> entitiesToDelete = new ArrayList<Long>();
        
        for (Map.Entry<Long, Boolean> entry : toRemove.entrySet()) {
            
            if(entry.getValue()){
                entitiesToDelete.add(entry.getKey());
            }
        }
        for (Board board : getBoards()) {
            if(entitiesToDelete.contains(board.getId())){
                boardsToDelete.add(board);
            }
        }

        boardService.removeBoards(boardsToDelete);
        toRemove.clear(); 
    }
    
    
    
    public NavigationBean getNavigationBean(){
        return this.navigationBean;
    }
    
    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }
}
