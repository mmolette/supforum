/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.controller;

import com.supinfo.supforum.entity.Message;
import com.supinfo.supforum.entity.Topic;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Mathieu
 */
@ManagedBean
@SessionScoped
public class TopicController {
    
    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;
    
    private Topic topic;
    private Message message;
    
    public TopicController(){
        
    }
    
    public Topic getTopic(){
        return this.topic;
    }
    
    public Message getMessage(){
        return this.message;
    }
    
    
    
    public NavigationBean getNavigationBean(){
        return this.navigationBean;
    }
    
    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }
}
