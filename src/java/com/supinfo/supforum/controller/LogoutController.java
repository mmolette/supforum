/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.controller;

import com.supinfo.supforum.util.SessionUtil;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mathieu
 */
@ManagedBean
@SessionScoped
public class LogoutController {
    
    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;
    
    public LogoutController(){
        
    }
    
    public String logout(){
        SessionUtil.removeUser();
        return navigationBean.redirectToLogin();
    }
    
    public NavigationBean getNavigationBean(){
        return this.navigationBean;
    }
    
    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }
}
