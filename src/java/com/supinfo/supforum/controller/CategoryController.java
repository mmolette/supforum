/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.controller;

import com.supinfo.supforum.entity.Category;
import com.supinfo.supforum.service.CategoryService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Mathieu
 */
@ManagedBean
@SessionScoped
public class CategoryController {
    
    @EJB
    CategoryService categoryService;
    
    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;
    
    private List<Category> categories;
    private Category category = new Category();
    
    // Remove
    private Map<Long, Boolean> toRemove = new HashMap<Long, Boolean>();
    
    public CategoryController(){ 
    }
    
    public Category getCategory(){
        
        return this.category;
    }
    
    public Map<Long, Boolean> getToRemove(){
        return this.toRemove;
    }
    
    
    
    
    /* Methods */
    
    public List<Category> getCategories(){
        
        List<Category> categories = categoryService.getAllCategories();
        return categories;
    }
    
    public void newCategory(){
        categoryService.addCategory(category);
    }
    
    public void removeCategory(){
        System.out.println("removeCat");
        List<Long> entitiesToDelete = new ArrayList<Long>();
        for (Map.Entry<Long, Boolean> entry : toRemove.entrySet()) {
            
            if(entry.getValue()){
                entitiesToDelete.add(entry.getKey());
            }
        }

        categoryService.removeCategories(entitiesToDelete);
        toRemove.clear(); 
    }
    
    public NavigationBean getNavigationBean(){
        return this.navigationBean;
    }
    
    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }
}
