/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.controller;

import com.supinfo.supforum.entity.User;
import static com.supinfo.supforum.filter.AuthFilter.SESSION_USER;
import com.supinfo.supforum.service.UserService;
import com.supinfo.supforum.util.SessionUtil;
import java.io.IOException;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mathieu
 */
@ManagedBean
@SessionScoped
public class LoginController {
    
    @EJB
    UserService userService;
    
    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;
    
    private User user = new User();
    
    
    public LoginController() {
    }
    
    public void checkUser() throws IOException{
        
        HttpSession session = SessionUtil.getSession();
        if(session.getAttribute(SESSION_USER) != null){
            System.out.println("test");
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/auth/forum.xhtml");
        }
    }
    
    public String login(){
        
        HttpSession session = SessionUtil.getSession();
        String forward = "";
        
        try {
            user = userService.userExists(user.getUsername(), user.getPassword());
            
            if(user != null){
                
                session.setAttribute(SESSION_USER, user);
                forward = navigationBean.redirectToAuthForum();
            }else{

            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Invalid Login!",
                    "Please Try Again!"));
            forward = navigationBean.toLogin(); 
        }
        
        return forward;
    }
    
    
    public User getUser(){
        return this.user;
    }
    
    public NavigationBean getNavigationBean(){
        return this.navigationBean;
    }
    
    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }
    
}
